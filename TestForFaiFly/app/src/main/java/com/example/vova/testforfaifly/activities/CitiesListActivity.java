package com.example.vova.testforfaifly.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vova.testforfaifly.R;
import com.example.vova.testforfaifly.adapters.CitiesAdapter;
import com.example.vova.testforfaifly.constans.NetworkConstans;
import com.example.vova.testforfaifly.model.City;
import com.example.vova.testforfaifly.model.DBWrappers.CityInfoDBWrapper;
import com.example.vova.testforfaifly.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by vovan on 04.07.2017.
 */

public class CitiesListActivity extends BaseActivity implements CitiesAdapter.OnClickCityItem {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    private ArrayList<City> mCities = new ArrayList<>();
    private CitiesAdapter mAdapter;

    public static final String KEY_COUNTRY_NAME = "KEY_COUNTRY_NAME";
    private String mCountryName = "";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_cities_list;
    }

    @Override
    protected void initActivity() {

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                mCountryName = bundle.getString(KEY_COUNTRY_NAME, "");
            }
        }

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewCitiesListActivity);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        setData();
    }

    private void setData() {
        CityInfoDBWrapper wrapper = new CityInfoDBWrapper(getApplicationContext());
        if (wrapper.getAllCitiesByCountryName(mCountryName).isEmpty()) {
            if (!isOnline(this)) {
                Toast.makeText(this, R.string.textNoInternetConnection, Toast.LENGTH_SHORT).show();
                finish();
            } else {
                parseData(wrapper);
            }
        } else {
            getData(wrapper);
        }
    }

    private void getData(CityInfoDBWrapper wrapper) {
        mCities.clear();
        mCities.addAll(wrapper.getAllCitiesByCountryName(mCountryName));
        mAdapter = new CitiesAdapter(mCities);
        mAdapter.setOnClickCityItem(CitiesListActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void onClickCityItem(City city) {
        Intent intent = new Intent(CitiesListActivity.this, DetailCityActivity.class);
        intent.putExtra(DetailCityActivity.KEY_CITY_NAME, city.getCityName());
        startActivity(intent);

    }

    private void parseData(final CityInfoDBWrapper wrapper) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Utils.connectToData(NetworkConstans.URL_CONTRIES_AND_CITIES)) {
                    parse(NetworkConstans.URL_CONTRIES_AND_CITIES, wrapper);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.VISIBLE);
                            getData(wrapper);
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.textbadInternetConnection,
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            private void parse(String link, CityInfoDBWrapper wrapper) {
                JSONObject dataJsonObj;
                ArrayList<City> cities = new ArrayList<>();
                try {
                    dataJsonObj = new JSONObject(Utils.makeResultJSON(link));

                    JSONArray citiesArr = dataJsonObj.getJSONArray(mCountryName);

                    for (int i = 0; i < citiesArr.length(); i++) {
                        String city = (String) citiesArr.get(i);
                        if (!city.isEmpty()) {
                            cities.add(new City(mCountryName, city));
                        }
                    }
                    wrapper.addAllItems(cities);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
