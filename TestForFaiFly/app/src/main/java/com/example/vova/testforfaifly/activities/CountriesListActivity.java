package com.example.vova.testforfaifly.activities;

import android.content.Intent;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vova.testforfaifly.R;
import com.example.vova.testforfaifly.adapters.CountriesAdapter;
import com.example.vova.testforfaifly.constans.NetworkConstans;
import com.example.vova.testforfaifly.model.Country;
import com.example.vova.testforfaifly.model.DBWrappers.CountryInfoDBWrapper;
import com.example.vova.testforfaifly.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by vovan on 04.07.2017.
 */

public class CountriesListActivity extends BaseActivity implements CountriesAdapter.OnClickCountryItem {

    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    private ArrayList<Country> mCountries = new ArrayList<>();
    private CountriesAdapter mAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_countries_list;
    }

    @Override
    protected void initActivity() {

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewCountriesListActivity);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        setData();

    }

    private void setData() {
        CountryInfoDBWrapper wrapper = new CountryInfoDBWrapper(getApplicationContext());
        if (wrapper.getAllCountries().isEmpty()) {
            if (!isOnline(this)) {
                Toast.makeText(this, R.string.textNoInternetConnection, Toast.LENGTH_SHORT).show();
                finish();
            } else {
                parseData(wrapper);
            }
        } else {
            getData(wrapper);
        }
    }

    private void getData(CountryInfoDBWrapper wrapper) {
        mCountries.clear();
        mCountries.addAll(wrapper.getAllCountries());
        mAdapter = new CountriesAdapter(mCountries);
        mAdapter.setOnClickCountryItem(CountriesListActivity.this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);
        mProgressBar.setVisibility(View.GONE);
    }


    @Override
    public void onClickCountryItem(Country country) {
        Intent intent = new Intent(this, CitiesListActivity.class);
        intent.putExtra(CitiesListActivity.KEY_COUNTRY_NAME, country.getCountryName());
        startActivity(intent);
    }

    private void parseData(final CountryInfoDBWrapper wrapper) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                if (Utils.connectToData(NetworkConstans.URL_CONTRIES_AND_CITIES)) {
                    parse(NetworkConstans.URL_CONTRIES_AND_CITIES, wrapper);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setVisibility(View.VISIBLE);
                            getData(wrapper);
                        }
                    });

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), R.string.textbadInternetConnection, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            private void parse(String link, CountryInfoDBWrapper wrapper) {
                JSONObject dataJsonObj;
                ArrayList<Country> countries = new ArrayList<>();
                try {
                    dataJsonObj = new JSONObject(Utils.makeResultJSON(link));

                    Iterator<String> keys = dataJsonObj.keys();
                    while(keys.hasNext() ) {
                        String country = keys.next();
                        if (!country.isEmpty()) {
                            countries.add(new Country(country));
                        }
                    }
                    wrapper.addAllItems(countries);

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
