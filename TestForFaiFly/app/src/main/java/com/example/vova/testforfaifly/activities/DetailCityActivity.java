package com.example.vova.testforfaifly.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.vova.testforfaifly.R;
import com.example.vova.testforfaifly.adapters.DetailCitiesAdapter;
import com.example.vova.testforfaifly.constans.NetworkConstans;
import com.example.vova.testforfaifly.model.Geonames;
import com.example.vova.testforfaifly.network.api.WikiGeoNamesApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vovan on 04.07.2017.
 */

public class DetailCityActivity extends BaseActivity {

    private DetailCitiesAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;

    public static final String KEY_CITY_NAME = "KEY_CITY_NAME";
    public static final String USER_NAME = "vovanik";
    private String mCityName = "";
    private Geonames mGeonames;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail_city;
    }

    @Override
    protected void initActivity() {

        Intent intent = getIntent();
        if (intent != null) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                mCityName = bundle.getString(KEY_CITY_NAME, "");
            }
        }

        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerViewDetailCityListActivity);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(NetworkConstans.BASE_URL_GEONAMES_ORG)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WikiGeoNamesApi service = retrofit.create(WikiGeoNamesApi.class);

        service.getAll(mCityName, USER_NAME).enqueue(new Callback<Geonames>() {
            @Override
            public void onResponse(Call<Geonames> call, Response<Geonames> response) {
                mGeonames = response.body();
                mAdapter = new DetailCitiesAdapter(mGeonames);
                mRecyclerView.setAdapter(mAdapter);
                mRecyclerView.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<Geonames> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);
                Toast.makeText(DetailCityActivity.this, R.string.textOccurredDuringNetworking, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
