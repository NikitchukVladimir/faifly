package com.example.vova.testforfaifly.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vova.testforfaifly.R;
import com.example.vova.testforfaifly.model.City;

import java.util.ArrayList;

/**
 * Created by vovan on 04.07.2017.
 */

public class CitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnClickCityItem mClickCityItem = null;
    private ArrayList<City> mCities = new ArrayList<>();

    public CitiesAdapter(ArrayList<City> arrCities) {
        mCities = arrCities;
    }

    public void setOnClickCityItem(OnClickCityItem clickCityItem) {
        mClickCityItem = clickCityItem;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        View viewCitiesInfo = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_and_city_item, parent, false);
        viewHolder = new CitiesViewHolder(viewCitiesInfo);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CitiesViewHolder viewHolder = (CitiesViewHolder) holder;
        final City city = mCities.get(position);
        viewHolder.mTextView.setText(city.getCityName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickCityItem != null) {
                    mClickCityItem.onClickCityItem(city);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
//        Log.d("My", " mCities.size() -> " +  mCities.size());
        return mCities.size();
    }

    private class CitiesViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        public CitiesViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.textViewListItem);
        }
    }

    public interface OnClickCityItem {
        void onClickCityItem(City city);
    }
}
