package com.example.vova.testforfaifly.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vova.testforfaifly.R;
import com.example.vova.testforfaifly.model.Country;

import java.util.ArrayList;

/**
 * Created by vovan on 04.07.2017.
 */

public class CountriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private OnClickCountryItem mClickCountryItem = null;
    private ArrayList<Country> mCountries = new ArrayList<>();

    public CountriesAdapter(ArrayList<Country> arrCountries) {
        mCountries = arrCountries;
    }

    public void setOnClickCountryItem(OnClickCountryItem clickCountryItem) {
        mClickCountryItem = clickCountryItem;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        View viewCitiesInfo = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_and_city_item, parent, false);
        viewHolder = new CountriesViewHolder(viewCitiesInfo);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        CountriesViewHolder viewHolder = (CountriesViewHolder) holder;
        final Country country = mCountries.get(position);
        viewHolder.mTextView.setText(country.getCountryName());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickCountryItem != null) {
                    mClickCountryItem.onClickCountryItem(country);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCountries.size();
    }

    private class CountriesViewHolder extends RecyclerView.ViewHolder {

        TextView mTextView;

        public CountriesViewHolder(View itemView) {
            super(itemView);
            mTextView = (TextView) itemView.findViewById(R.id.textViewListItem);
        }
    }

    public interface OnClickCountryItem {
        void onClickCountryItem(Country country);
    }
}
