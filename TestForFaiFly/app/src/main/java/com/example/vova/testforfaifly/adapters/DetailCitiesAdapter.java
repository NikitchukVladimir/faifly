package com.example.vova.testforfaifly.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.vova.testforfaifly.R;
import com.example.vova.testforfaifly.model.Geoname;
import com.example.vova.testforfaifly.model.Geonames;

/**
 * Created by vovan on 04.07.2017.
 */

public class DetailCitiesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String HTTP = "http://";
    private Geonames mGeonames = new Geonames();
    private Context mContext;

    public DetailCitiesAdapter(Geonames geonames) {
        mGeonames = geonames;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;

        View viewCitiesInfo = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.detail_city_item, parent, false);
        viewHolder = new DetailCitiesViewHolder(viewCitiesInfo);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DetailCitiesViewHolder viewHolder = (DetailCitiesViewHolder) holder;

        final Geoname geoname = mGeonames.getGeonames().get(position);
        viewHolder.mTextViewTitle.setText(geoname.getTitle());
        viewHolder.mTextViewSummary.setText(geoname.getSummary());
        viewHolder.mButtonWikiUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dataLink = geoname.getWikipediaUrl();
                if ((!dataLink.contains(HTTP))) {
                    dataLink = HTTP + dataLink;
                }
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataLink));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGeonames.getGeonames().size();
    }

    private class DetailCitiesViewHolder extends RecyclerView.ViewHolder {

        TextView mTextViewTitle;
        TextView mTextViewSummary;
        Button mButtonWikiUrl;

        public DetailCitiesViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            mTextViewTitle = (TextView) itemView.findViewById(R.id.textViewTitleDetailCityItem);
            mTextViewSummary = (TextView) itemView.findViewById(R.id.textViewSummaryDetailCityItem);
            mButtonWikiUrl = (Button) itemView.findViewById(R.id.textViewWikipediaUrlDetailCityItem);
        }
    }
}
