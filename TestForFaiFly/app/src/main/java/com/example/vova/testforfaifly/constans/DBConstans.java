package com.example.vova.testforfaifly.constans;

/**
 * Created by vovan on 04.07.2017.
 */

public class DBConstans {

    public static final String DB_NAME = "note_db";
    public static final int DB_VERSION = 1;

    public static final class CountriesTable {
        public static final String TABLE_NAME = "CountriesTable";

        public static final class Cols {
            public static final String COUNTRIES_INFO_FIELD_ID = "_id";
            public static final String COUNTRIES_INFO_FIELD_NAME = "_name";
        }
    }

    public static final class CitiesTable {
        public static final String TABLE_NAME = "CitiesInfo";

        public static final class Cols {
            public static final String CITIES_INFO_FIELD_ID = "_id";
            public static final String CITIES_INFO_FIELD_COUNTRY_NAME = "_country_name";
            public static final String CITIES_INFO_FIELD_NAME = "_name";
        }
    }
}
