package com.example.vova.testforfaifly.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.vova.testforfaifly.constans.DBConstans;
import com.example.vova.testforfaifly.constans.DBConstans.CitiesTable;
import com.example.vova.testforfaifly.constans.DBConstans.CountriesTable;

/**
 * Created by vovan on 04.07.2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBConstans.DB_NAME, null, DBConstans.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("CREATE TABLE " + CountriesTable.TABLE_NAME
                + " (" + CountriesTable.Cols.COUNTRIES_INFO_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CountriesTable.Cols.COUNTRIES_INFO_FIELD_NAME + " TEXT); ");


        sqLiteDatabase.execSQL("CREATE TABLE " + CitiesTable.TABLE_NAME
                + " (" + CitiesTable.Cols.CITIES_INFO_FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + CitiesTable.Cols.CITIES_INFO_FIELD_COUNTRY_NAME + " TEXT NOT NULL, "
                + CitiesTable.Cols.CITIES_INFO_FIELD_NAME + " TEXT); ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
