package com.example.vova.testforfaifly.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.vova.testforfaifly.constans.DBConstans.CitiesTable;

/**
 * Created by vovan on 04.07.2017.
 */

public class City extends BaseEntity {

    private String countryName;
    private String cityName;

    public City(String countryName, String cityName) {
        this.countryName = countryName;
        this.cityName = cityName;
    }

    public City(Cursor cursor) {
        setId(cursor.getLong(cursor.getColumnIndex(CitiesTable.Cols.CITIES_INFO_FIELD_ID)));
        countryName = cursor.getString(cursor.getColumnIndex(CitiesTable.Cols.CITIES_INFO_FIELD_COUNTRY_NAME));
        cityName = cursor.getString(cursor.getColumnIndex(CitiesTable.Cols.CITIES_INFO_FIELD_NAME));
    }

    public String getCityName() {
        return cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(CitiesTable.Cols.CITIES_INFO_FIELD_COUNTRY_NAME, getCountryName());
        values.put(CitiesTable.Cols.CITIES_INFO_FIELD_NAME, getCityName());
        return values;
    }
}
