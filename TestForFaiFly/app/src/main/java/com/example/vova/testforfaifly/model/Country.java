package com.example.vova.testforfaifly.model;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.vova.testforfaifly.constans.DBConstans.CountriesTable;

/**
 * Created by vovan on 04.07.2017.
 */

public class Country extends BaseEntity{

    private String countryName;

    public Country(String countryName) {
        this.countryName = countryName;
    }

    public Country(Cursor cursor) {
        setId(cursor.getLong(cursor.getColumnIndex(CountriesTable.Cols.COUNTRIES_INFO_FIELD_ID)));
        countryName = cursor.getString(cursor.getColumnIndex(CountriesTable.Cols.COUNTRIES_INFO_FIELD_NAME));
    }

    public String getCountryName() {
        return countryName;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(CountriesTable.Cols.COUNTRIES_INFO_FIELD_NAME, getCountryName());
        return values;
    }
}
