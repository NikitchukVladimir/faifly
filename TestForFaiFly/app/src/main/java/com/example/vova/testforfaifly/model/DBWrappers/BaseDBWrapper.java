package com.example.vova.testforfaifly.model.DBWrappers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.vova.testforfaifly.db.DBHelper;
import com.example.vova.testforfaifly.model.BaseEntity;

import java.util.ArrayList;

/**
 * Created by vovan on 04.07.2017.
 */

public class BaseDBWrapper<T extends BaseEntity> {

    private DBHelper mDBHelper;
    private String mStrTableName;

    public BaseDBWrapper(Context context, String strTableName) {
        mDBHelper = new DBHelper(context);
        mStrTableName = strTableName;
    }

    public String getTableName() {
        return mStrTableName;
    }

    public SQLiteDatabase getWritable() {
        return mDBHelper.getWritableDatabase();
    }

    public SQLiteDatabase getReadable() {
        return mDBHelper.getReadableDatabase();
    }

    public void addAllItems(ArrayList<T> items) {
        SQLiteDatabase database = getWritable();
        try {
            database.beginTransaction();
            for (T item : items) {
                database.insert(getTableName(), null, item.getContentValues());
            }
            database.setTransactionSuccessful();
        } finally {
            database.endTransaction();
            database.close();
        }
    }
}
