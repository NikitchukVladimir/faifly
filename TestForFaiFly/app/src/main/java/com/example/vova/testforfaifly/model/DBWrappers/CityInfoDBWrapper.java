package com.example.vova.testforfaifly.model.DBWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vova.testforfaifly.constans.DBConstans.CitiesTable;
import com.example.vova.testforfaifly.model.City;

import java.util.ArrayList;

/**
 * Created by vovan on 04.07.2017.
 */

public class CityInfoDBWrapper extends BaseDBWrapper<City> {

    public CityInfoDBWrapper(Context context) {
        super(context, CitiesTable.TABLE_NAME);
    }

    @Override
    public void addAllItems(ArrayList<City> citiesItems) {
        super.addAllItems(citiesItems);
    }

    public ArrayList<City> getAllCitiesByCountryName(String countryName) {
        ArrayList<City> arrResult = new ArrayList<>();
        SQLiteDatabase database = getReadable();
        String strRequest = CitiesTable.Cols.CITIES_INFO_FIELD_COUNTRY_NAME + "=?";
        String arrArgs[] = new String[]{countryName};
        Cursor cursor = database.query(getTableName(), null, strRequest, arrArgs, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    arrResult.add(new City(cursor));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.close();
        }
        return arrResult;
    }
}
