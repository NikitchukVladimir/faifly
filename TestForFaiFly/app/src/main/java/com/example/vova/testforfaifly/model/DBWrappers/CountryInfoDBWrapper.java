package com.example.vova.testforfaifly.model.DBWrappers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vova.testforfaifly.constans.DBConstans.CountriesTable;
import com.example.vova.testforfaifly.model.Country;

import java.util.ArrayList;

/**
 * Created by vovan on 04.07.2017.
 */

public class CountryInfoDBWrapper extends BaseDBWrapper<Country> {

    public CountryInfoDBWrapper(Context context) {
        super(context, CountriesTable.TABLE_NAME);
    }

    @Override
    public void addAllItems(ArrayList<Country> countryItems) {
        super.addAllItems(countryItems);
    }

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> arrResult = new ArrayList<>();
        SQLiteDatabase database = getReadable();
        Cursor cursor = database.query(getTableName(), null, null, null, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    Country country = new Country(cursor);
                    arrResult.add(country);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            database.close();
        }
        return arrResult;
    }
}
