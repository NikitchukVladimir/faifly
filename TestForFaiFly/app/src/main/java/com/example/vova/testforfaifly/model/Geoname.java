package com.example.vova.testforfaifly.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Geoname {

    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("elevation")
    @Expose
    private int elevation;
    @SerializedName("geoNameId")
    @Expose
    private int geoNameId;
    @SerializedName("lng")
    @Expose
    private double lng;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("rank")
    @Expose
    private int rank;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("wikipediaUrl")
    @Expose
    private String wikipediaUrl;
    @SerializedName("feature")
    @Expose
    private String feature;

    public String getSummary() {
        return summary;
    }

    public int getElevation() {
        return elevation;
    }

    public int getGeoNameId() {
        return geoNameId;
    }

    public double getLng() {
        return lng;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public int getRank() {
        return rank;
    }

    public String getLang() {
        return lang;
    }

    public String getTitle() {
        return title;
    }

    public double getLat() {
        return lat;
    }

    public String getWikipediaUrl() {
        return wikipediaUrl;
    }

    public String getFeature() {
        return feature;
    }

}
