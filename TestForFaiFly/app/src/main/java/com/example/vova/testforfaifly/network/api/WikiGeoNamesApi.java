package com.example.vova.testforfaifly.network.api;

import com.example.vova.testforfaifly.model.Geonames;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by vovan on 04.07.2017.
 */

public interface WikiGeoNamesApi {

    @Headers("Accept: application/json")
    @GET("/wikipediaSearchJSON")
    Call<Geonames> getAll(@Query("q") String qGeoname, @Query("username") String userName);
}
